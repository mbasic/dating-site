'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _mysql = require('mysql');

var _mysql2 = _interopRequireDefault(_mysql);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

var datingConnection = _mysql2.default.createConnection({
	host: "localhost",
	user: "root",
	password: "justdoit2",
	database: "dating_site"
});

datingConnection.connect();

/* GET home page. */
router.get('/', function (req, res) {
	res.render('home');
});

router.get('/home', function (req, res) {
	datingConnection.query('SELECT * FROM users WHERE user_id=?', req.user, function (err, result) {
		console.log(req.user);
		//if(err) throw err;
		if (typeof req.user == "undefined") {
			res.render("home", { user: result });
		}
	});
	//res.render('home');
});

router.get('/about', function (req, res) {
	datingConnection.query('SELECT * FROM users WHERE user_id=?', req.user, function (err, result) {
		console.log(req.user);
		//if(err) throw err;
		if (typeof req.user == "undefined" || typeof req.user != "undefined") {
			res.render("about", { user: result });
		}
	});
});

module.exports = router;
