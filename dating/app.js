'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _morgan = require('morgan');

var _morgan2 = _interopRequireDefault(_morgan);

var _cookieParser = require('cookie-parser');

var _cookieParser2 = _interopRequireDefault(_cookieParser);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _bcryptNodejs = require('bcrypt-nodejs');

var _bcryptNodejs2 = _interopRequireDefault(_bcryptNodejs);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _expressSession = require('express-session');

var _expressSession2 = _interopRequireDefault(_expressSession);

var _passportLocal = require('passport-local');

var _passportLocal2 = _interopRequireDefault(_passportLocal);

var _mysql = require('mysql');

var _mysql2 = _interopRequireDefault(_mysql);

var _multer = require('multer');

var _multer2 = _interopRequireDefault(_multer);

var _mime = require('mime');

var _mime2 = _interopRequireDefault(_mime);

var _shortid = require('shortid');

var _shortid2 = _interopRequireDefault(_shortid);

var _index = require('./routes/index');

var _index2 = _interopRequireDefault(_index);

var _users = require('./routes/users');

var _users2 = _interopRequireDefault(_users);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var connection = _mysql2.default.createConnection({
	host: "localhost",
	user: "root",
	password: "justdoit2",
	database: "dating_site"
});

connection.connect();

var storage = _multer2.default.diskStorage({
	destination: function destination(req, file, cb) {
		req.image_url = './public/profile_photos';
		cb(null, req.image_url);
	},
	filename: function filename(req, file, cb) {
		var filename = _shortid2.default.generate(); //+"."+ mime.extension(file.mimetype);
		req.image_url = '../profile_photos/' + filename;
		cb(null, filename);
	}
});

var upload = (0, _multer2.default)({ storage: storage });

var app = (0, _express2.default)();
var server = require('http').createServer(app);
var socketIo = require('socket.io')(server);
//var socketIo = require('socket.io').listen(server);
var chat_users = {};

server.listen(8080);

// view engine setup
app.set('views', _path2.default.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use((0, _morgan2.default)('dev'));
app.use(_bodyParser2.default.json());
app.use(_bodyParser2.default.urlencoded({ extended: false }));
app.use((0, _cookieParser2.default)());
app.use((0, _expressSession2.default)({ secret: "marina secret key", resave: true, saveUninitialized: false }));
app.use(_express2.default.static(_path2.default.join(__dirname, 'public')));

//Passport
app.use(_passport2.default.initialize());
app.use(_passport2.default.session());
_passport2.default.use("local-signin", new _passportLocal.Strategy(function (username, password, done) {
	connection.query('SELECT user_id, username, password FROM users WHERE username = ?', username, function (err, rows, columns) {
		if (err) throw err;
		_bcryptNodejs2.default.compare(password, rows[0].password, function (err, same) {
			if (same) return done(null, { id: rows[0].user_id });else return done(null, false);
		});
	});
}));
_passport2.default.use("local-signup", new _passportLocal.Strategy({ passReqToCallback: true }, function (req, username, password, done) {
	connection.query('SELECT * FROM users WHERE username = ?', username, function (err, result) {
		if (err) throw err;
		if (result) {
			console.log("Username taken");return;
		};
	});
	connection.query('SELECT * FROM users WHERE email = ?', req.body.email, function (err, result) {
		if (err) throw err;
		if (result) {
			console.log("This email is already in use!");return;
		}
	});
	_bcryptNodejs2.default.hash(password, null, null, function (err, hash) {
		var q = req.body;
		if (err) throw err;else connection.query('INSERT INTO users(name, surname, country, gender, age, email, username, password, description, image_url) VALUES (?,?,?,?,?,?,?,?,?,?)', [q.name, q.surname, q.country, q.gender, q.age, q.email, username, hash, q.description, req.image_url], function (err, result) {
			if (err) throw err;
			return done(null, { id: result.insertId });
		});
	});
}));

_passport2.default.serializeUser(function (user, done) {
	//odredujemo sto spremamo u sesiju
	done(null, user.id);
});

_passport2.default.deserializeUser(function (user, done) {
	//rehidracija, pomoci id retreiveamo informacije iz sesije
	done(null, user); // user ce bit zakvacen za req.user req.logout() za logout
});

app.post('/signup', upload.any(), _passport2.default.authenticate('local-signup', {
	successRedirect: '/users/profile',
	failureRedirect: '/users/create'
}));

app.post('/signin', _passport2.default.authenticate('local-signin', {
	successRedirect: '/users/profile',
	failureRedirect: '/home/#sign_in'
}));

app.post('/edit_profile', upload.any(), function (req, res) {
	var q = req.body;
	if (req.image_url) {
		connection.query('UPDATE users SET image_url=? WHERE user_id=?', [req.image_url, q.user_id], function (err, result) {
			if (err) throw err;
		});
	}
	connection.query('UPDATE users SET age=?, country=?, description=? WHERE user_id=?', [q.age, q.country, q.description, q.user_id], function (err, result) {
		if (err) throw err;
		res.redirect('/users/profile');
	});
});

app.get('/logout', function (req, res) {
	req.logout();
	res.cookie('connect.sid', { path: '/' }, { maxAge: 0, expires: 0 }).redirect('/home');
});

socketIo.sockets.on('connection', function (socket) {
	socket.on('new user', function (data, callback) {
		callback(true);

		socket.username = data;
		chat_users[socket.username] = socket;
		updateUsernames();
	});

	function updateUsernames() {
		socketIo.sockets.emit('usernames', Object.keys(chat_users));
	}

	socket.on('send message', function (data, callback) {
		var msg = data.trim();
		if (msg.substr(0, 1) !== ' ') {
			var indx = msg.indexOf(' ');
			if (indx !== -1) {
				var receiver_id = msg.substring(0, indx);
				var msg = msg.substring(indx + 1);
				connection.query('SELECT username FROM users WHERE user_id=?', receiver_id, function (err, result) {
					if (err) throw err;
					var receiver_name = result[0].username;
					if (receiver_name in chat_users) {
						connection.query('SELECT user_id, name, surname FROM users WHERE username=?', socket.username, function (err, row) {
							if (err) throw err;
							var sender_id = row[0].user_id;
							var sender_n = row[0].name;
							var sender_s = row[0].surname;
							socketIo.to(chat_users[receiver_name].id).emit('new message', { msg: msg, user: socket.username, sender: sender_id, sender_name: sender_n, sender_surname: sender_s });
							chat_users[socket.username].emit('new message', { msg: msg, user: "Me", sender: receiver_id });
						});
					} else {
						callback('User is offline!');
					}
				});
			} else {
				callback('Please enter a message');
			}
		}
	});

	socket.on('disconnect', function (data) {
		if (!socket.username) return;
		delete chat_users[socket.username];
		updateUsernames();
	});
});

app.use('/', _index2.default);
app.use('/users', _users2.default);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.render('error');
});

module.exports = app;
