'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _mysql = require('mysql');

var _mysql2 = _interopRequireDefault(_mysql);

var _bcryptNodejs = require('bcrypt-nodejs');

var _bcryptNodejs2 = _interopRequireDefault(_bcryptNodejs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

var datingConnection = _mysql2.default.createConnection({
	host: "localhost",
	user: "root",
	password: "justdoit2",
	database: "dating_site"
});

datingConnection.connect();

/* GET users listing. */
/*router.get('/', (req, res) => {
    datingConnection.query('SELECT * FROM users', function(err, records) {
		if (err) {
			throw err;
		}
		res.render("users", {users:records});
	});
});*/

router.get('/create', function (req, res) {
	res.render('user_form');
});

router.get('/isusernametaken/:username', function (req, res) {
	datingConnection.query('SELECT username FROM users WHERE username=?', req.params.username, function (err, istaken) {
		if (err) {
			throw err;
		}
		if (istaken.length) {
			console.log(istaken);
			res.send(true);
		} else {
			res.send(false);
		}
	});
});

router.post('/checkuser', function (req, res) {
	datingConnection.query('SELECT username, password FROM users WHERE username=?', req.body.username, function (err, result) {
		if (err) throw err;else if (result.length) {
			res.end("exist");
		} else {
			res.end("true");
		}
	});
});

router.get('/profile', function (req, res) {
	datingConnection.query('SELECT * FROM users JOIN countries ON users.country=countries.abbrev_id WHERE user_id=?', req.user, function (err, result) {
		if (err) throw err;
		res.render("profile", { user: result });
	});
});

router.get('/search/:search_input?', function (req, res) {
	var search = req.params.search_input;
	var sql = _mysql2.default.format("SELECT country_name FROM users JOIN countries ON users.country=countries.abbrev_id WHERE country_name LIKE ? GROUP BY country_name", [search + "%"]);
	datingConnection.query(sql, req.params.country_name, function (err, result) {
		if (err) throw err;
		if (result) {
			res.send(result);
		}
	});
});

router.get('/all', function (req, res) {
	datingConnection.query('SELECT user_id, username FROM users WHERE user_id=?', req.user, function (err, result) {
		if (typeof req.user != "undefined") {
			var sql = _mysql2.default.format('SELECT user_id, name, surname, gender, age, country_name, description, image_url FROM users JOIN countries ON users.country=countries.abbrev_id WHERE user_id NOT LIKE ?', [req.user]);
			datingConnection.query(sql, req.user, function (err, records) {
				if (err) throw err;
				res.render("all", { user: result, users: records });
			});
		}
	});
});

router.get('/filter', function (req, res) {
	var country = req.query.country;
	var gender = req.query.gender;
	var baseQuery = 'SELECT user_id, name, surname, gender, age, country_name, description, image_url FROM users JOIN countries ON users.country=countries.abbrev_id WHERE user_id NOT LIKE ?';

	datingConnection.query('SELECT user_id, username FROM users WHERE user_id=?', req.user, function (err, result) {
		if (typeof req.user != "undefined") {
			if (country == "" && gender == "Gender") {
				var sql = _mysql2.default.format(baseQuery, [req.user]);
				datingConnection.query(sql, req.user, function (err, records) {
					if (err) throw err;
					res.send(records);
				});
			} else if (country == "") {
				var sql = _mysql2.default.format(baseQuery + ' ' + 'AND gender LIKE ?', [req.user, gender]);
				datingConnection.query(sql, req.user, function (err, records) {
					if (err) throw err;
					res.send(records);
				});
			} else if (gender == "Gender") {
				var sql = _mysql2.default.format(baseQuery + ' ' + 'AND country_name LIKE ?', [req.user, country + "%"]);
				datingConnection.query(sql, req.user, function (err, records) {
					if (err) throw err;
					res.send(records);
				});
			} else {
				var sql = _mysql2.default.format(baseQuery + ' ' + 'AND country_name LIKE ? AND gender LIKE ?', [req.user, country + "%", gender]);
				datingConnection.query(sql, req.user, function (err, records) {
					if (err) throw err;
					res.send(records);
				});
			}
		}
	});
});

router.get('/profile/edit', function (req, res) {
	datingConnection.query('SELECT user_id, username, name, surname, gender, age, country, country_name, description, image_url FROM users JOIN countries ON users.country=countries.abbrev_id WHERE user_id=?', req.user, function (err, result) {
		if (err) throw err;
		if (typeof req.user != "undefined") {
			res.render("edit_profile", { user: result });
		}
	});
});

router.get('/profile/delete', function (req, res) {
	var user = req.user;
	datingConnection.query('DELETE FROM users WHERE user_id=?', req.user, function (err, result) {
		res.redirect('/logout');
	});
});

exports.default = router;
